<?php

namespace App\Http\Controllers;

use App\Thread;
use Illuminate\Http\RedirectResponse;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index','show']);
    }

    /**
     * @param  $channelId
     * @param  Thread $thread
     * @return RedirectResponse
     */
    public function store($channelId, Thread $thread)
    {
        $this->validate(
            request(),
            [
                'body' => 'required'
            ]
        );

        $thread->addReply(
            [
                'body' => request('body'),
                'user_id' => auth()->id()
            ]
        );

        return back();
    }
}
