<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ParticipateInForumTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldNotAllowUnAuthenticatedUserAddReplies()
    {
        $thread = create('App\Thread');
        $this->withExceptionHandling()
           ->post("{$thread->path()}/replies", [])
           ->assertRedirect('/login');
    }
    public function testItShouldAllowAnAuthenticatedUserParticipateInForumThread()
    {
        // Given we have a authenticated user
        $this->signIn();
        // And an existing thread
        $thread = create('App\Thread');
        // When the user adds a reply to the thread
        $reply = create('App\Reply');
        $this->post(
            $thread->path() . '/replies',
            $reply->toArray()
        )->assertRedirect();
        // Then their reply should be visible on the page.
        $this->get($thread->path())->assertSee($reply->body);
    }

    public function testItShouldReplyWithABody()
    {
        $this->withExceptionHandling()->signIn();
        $thread = create('App\Thread');
        $this->post(
            $thread->path() . '/replies',
            ['body' => null]
        )->assertRedirect();
    }
}
