<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReadThreadsTest extends TestCase
{
    use DatabaseMigrations;

    private $thread;

    public function setUp()
    {
        parent::setUp();
        $this->thread = factory('App\Thread')->create();
    }

    public function testItShouldAllowUserBrowseThreads()
    {
        $response = $this->get('/threads');
        $response->assertSee($this->thread->title);
    }

    public function testItShouldAllowUserSeeASingleThread()
    {
        $response = $this->get($this->thread->path());
        $response->assertSee($this->thread->title);
    }

    public function testItShouldAllowUserReadAndRepliesThatAreAssociatedWithAThread()
    {
        // Given we have a thread
        // And that thread includes replies
        $reply = factory('App\Reply')
            ->create(
                [
                    'thread_id' => $this->thread->id
                ]
            );
        // When we visit a thread page
        // Then we should see the replies
        $this->get($this->thread->path())
                ->assertSee($reply->body)
                ->assertSee($reply->owner->name)
                ->assertSee($reply->created_at->diffForHumans());
    }

    public function testItShouldAllowUserFilterThreadAccordingToATag()
    {
        $channel = create('App\Channel');
        $threadNotInChannel = create('App\Thread');
        $threadInChannel = create(
            'App\Thread',
            ['channel_id' => $channel->id]
        );
        $this->get('/threads/' . $channel->slug)
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }
}
