<?php

namespace Tests\Feature;

use App\Thread;
use Illuminate\Auth\AuthenticationException;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class CreateThreadsTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldNotAllowUnauthenticatedUserSeeCreateThreadPage()
    {
        $this->withExceptionHandling();
        $this->get('/threads/create')->assertRedirect('/login');
    }

    public function testItShouldNotAllowUnauthenticatedUserCreateAThread()
    {
        $this->expectException(AuthenticationException::class);
        // When we hit the endpoint to creat a new thread
        /**
         * @var Thread $thread
         */
        $thread = make('App\Thread');
        $this->post('/threads', $thread->toArray());
    }

    public function testItShouldBeAbleAnUserCreateNewForumThreads()
    {
        // Given we have a signed user
        $this->signIn();

        // When we hit the endpoint to creat a new thread
        /**
         * @var Thread $thread
         */
        $thread = make('App\Thread');
        $response = $this->post('/threads', $thread->toArray());

        // Then, when we visit the thread page

        $this->get($response->headers->get('Location'))
            ->assertSee($thread->body)
            ->assertSee($thread->title);
        // Whe should see the new thread.
    }

    public function testItShouldRequireATitle()
    {
        $this->publishThread(['title' => null])
        ->assertSessionHasErrors('title');
    }

    public function testItShouldRequireABody()
    {
        $this->publishThread(['body' => null])
            ->assertSessionHasErrors('body');
    }

    public function testItShouldRequiresAValidChannel()
    {
        factory('App\Channel', 2);
        $this->publishThread(['channel_id' => null])
            ->assertSessionHasErrors('channel_id');
        $this->publishThread(['channel_id' => 999])
            ->assertSessionHasErrors('channel_id');
    }

    private function publishThread(array $overrides = [])
    {
        $this->withExceptionHandling()->signIn();
        $thread = make('App\Thread', $overrides);
        return $this->post('/threads', $thread->toArray());
    }
}
