<?php

namespace Tests\Unit;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ReplyTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testItShouldHaveOwner()
    {
        $reply = factory('App\Reply')->create();

        $this->assertInstanceOf(User::class, $reply->owner);
    }
}
