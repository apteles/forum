<?php

namespace Tests\Unit;

use App\Thread;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ChannelTest extends TestCase
{
    use DatabaseMigrations;

    public function testItShouldHaveThreads()
    {
        $channel = create('App\Channel');
        create('App\Thread', ['channel_id' => $channel->id]);
        $this->assertInstanceOf(Thread::class, $channel->threads->first());
    }
}
