module.exports = {
    '**/*.php': ['php vendor/bin/phpcs --standard=ruleset.xml --no-cache'],
    '*Test.php': ['php vendor/bin/phpunit --stop-on-failure --do-not-cache-result']
};
